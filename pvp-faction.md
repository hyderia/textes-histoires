# Le PvP/Faction
Un beau jour, au pays minecraftien de Hyperia, un PvP/Faction apparait. C'était la grande hype, tout le monde (ou presque) commence le PvP/Faction avec une seule et unique phrase en tête: **It's to kill or be killed**.

Dans ce monde sans merci, tout est permis: les bases truffées de pièges, du TP kill (assassinage de la personne après un TP fisheux) ou encore la surprise. Les arnaques et les faux membres de faction aussi. Le but étant de **faire régner l'anarchie** et faire des crimes: du vol, du grief et du meurtre.

C'est un monde de terreur. Tout le monde regarde ses arrières. Tout le monde fait la course à l'armement: épées, armures, enchantements, potions. C'est l'anarchie: On se fait piéger sans savoir. On signe son arrêté de mort sans le savoir. On cache ses items avec le plus de valeur. Les ruines sont partout. Des anciennes bases vidées seront partout. On fuit constamenent l'enemi.

Mais dans ce monde rempli d'injustices, une personne, une faction saura tirer son épingle du jeu et prospérer. Mais ce sera qui ?

Lancez-vous dans cette aventure remplie de danger. Soyez braves, combattez et tuez du monde. Réduisez à néant une faction ou laissez la un peu de merci. Transformez des bases remplies de vie en cimetière hanté par les âmes des précédents habitants. Recrutez du monde mais ne vous faites pas avoir par des petits malins à tout voler.

Créez des alliances ou faites-vous des enemis à anéantir à tout prix...

Dans ce monde anarchique, bonne chance à toi, jeune padawan.
